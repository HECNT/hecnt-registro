var mysql      = require('mysql');
var connection = require('./main');

module.exports.test = function () {
  return new Promise(function(resolve, reject) {
    connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
      if (error) throw error;
      console.log('The solution is: ', results[0].solution);
      resolve(results)
    });
  });
}
