use registro;

CREATE TABLE nivel
(
  nivel_id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  PRIMARY KEY (nivel_id)
);

CREATE TABLE perfil
(
  perfil_id INT NOT NULL AUTO_INCREMENT,
  nivel_id INT NOT NULL,
  estatus INT NOT NULL,
  PRIMARY KEY (perfil_id),
  FOREIGN KEY (nivel_id) REFERENCES nivel(nivel_id)
);

CREATE TABLE usuario
(
  usuario_id INT NOT NULL AUTO_INCREMENT,
  usuario INT NOT NULL,
  password VARCHAR(50) NOT NULL,
  perfil_id INT NOT NULL,
  estatus INT NOT NULL,
  PRIMARY KEY (usuario_id),
  FOREIGN KEY (perfil_id) REFERENCES perfil(perfil_id)
);

CREATE TABLE ingresar
(
  ingresar_id INT NOT NULL AUTO_INCREMENT,
  usuario_id INT NOT NULL,
  fecha DATE NOT NULL,
  PRIMARY KEY (ingresar_id),
  FOREIGN KEY (usuario_id) REFERENCES usuario(usuario_id)
);

SELECT * FROM nivel
insert into nivel (nombre) values ('ALUMNO')

SELECT * FROM perfil
insert into perfil (nivel_id,estatus) values (1,1)

SELECT * FROM usuario
insert into usuario (usuario,password,perfil_id,estatus) values (1,'IPN-NACIONAL', 1, 1)
